import React from 'react';
import Week from './week.js';
import { Link } from 'react-router-dom';

class Sidebar extends React.Component {
	constructor(props){
		super(props);
		this.state = { 
			props: this.props
		}
	}

	componentWillReceiveProps(nextProps) {
		this.setState({ props: nextProps });
	}

	isSame(a,b){
		return a+"" === b+""
	}

	render(){
		if(!this.state.props.isLoaded){
			return (
				<div className="sidebar">
					<h3 className="title">
						Loading
					</h3>
				</div>
			)
		} else {
			return (
				<div className="sidebar">
					<div className="scroller">
						<h3 className="title">
							<Link to="/">{this.state.props.title}</Link>
						</h3>
						{this.state.props.weeks && this.state.props.weeks.map(function(week){
							return <Week key={week.ID} isSelected={this.isSame(this.state.props.selectedWeek,week.ID)} {...week} />
						}, this)}
						<hr className="spacer hidden" />
					</div>
				</div>
			)
		}
	}
}

export default Sidebar;