import React from 'react';
//import { BrowserRouter as Router, Route } from 'react-router-dom';
import Sidebar from './sidebar.js';
import DetailView from './detail-view.js';
import '../../styles/mealplan-detail.css';
import '../../styles/sidebar.css';

class MealPlanDetail extends React.Component {
	constructor(props){
		super(props);
		this.state = { 
			props: this.props
		}
	}

	componentWillReceiveProps(nextProps) {
		this.getSelectedMeal(nextProps.match.params.mealid);
		this.getSelectedReflection(nextProps.match.params.mealid);
	}

	componentDidMount() {
		if(!this.state.isLoaded && this.state.plan == null){
			fetch(`https://meal.milkmanmatty.com.au/umbraco/api/Data/GetMealFromWeek?w=${this.props.match.params.weekid}`)
			.then(res => res.json())
			.then(
				(result) => {
					this.setState({
						isLoaded: true,
						plan: result
					});
					this.getSelectedMeal();
					this.getSelectedReflection();
				},
				(error) => {
					this.setState({
						isLoaded: false,
						plan: null
					});
					console.log(error);
				}
			)
		}
	}

	getSelectedMeal(id){
		if(this.state.isLoaded){
			var result;
			for (let week of this.state.plan.Weeks){
				for (let day of week.Days){
					for (let meal of day.Meals){
						if(meal.ID+"" === ""+id){
							result = meal;
							break;
						}
					}
					if(result){break;}
				}
				if(result){break;}
			}
			this.setState({ selectedMeal: result });
		}
	}
	getSelectedReflection(id){
		if(this.state.isLoaded){
			var result;
			for (let week of this.state.plan.Weeks){
				for (let r of week.Reflections){
					if(r.ID+"" === ""+id){
						result = r;
						break;
					}
				}
				if(result){break;}
			}
			this.setState({ selectedReflection: result });
		}
	}

    render(){
		if(!this.state.isLoaded){
			return (
				<div className="details_container">
					<Sidebar
						isLoaded={false}
						weeks={null}
					/>
					<DetailView />
				</div>
			)
		} else {
			return (
				<div className="details_container">
					<Sidebar
						isLoaded={true}
						title={this.state.plan.Title}
						weeks={this.state.plan.Weeks}
						selectedWeek={this.props.match.params.weekid}
					/>
					<DetailView 
						isLoaded={true}
						meal={this.state.selectedMeal}
						reflect={this.state.selectedReflection}
					/>
				</div>
			)
		}
	}
}

export default MealPlanDetail;
