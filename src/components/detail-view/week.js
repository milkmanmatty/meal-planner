import React from 'react';
import Day from './day';
import Collapse from '@kunukn/react-collapse';
import { Link } from 'react-router-dom';

class Week extends React.Component {
	constructor(props){
		super(props);
		this.state = { 
			props: this.props,
			isOpen: props.isSelected
		}
	}

	componentWillReceiveProps(nextProps) {
		this.setState({ props: nextProps });
	}

	onToggle = () => this.setState({isOpen: !this.state.isOpen});

	render(){
		return (
			<div className={"week "+(this.state.isOpen ? "expanded" : "collapsed")+" "+this.state.props.isSelected}>
				<h4 onClick={this.onToggle}>{this.state.props.Title}</h4>
				<Collapse
					elementType="section"
					isOpen={this.state.isOpen}
					render={state => (
						<div>
							{this.state.props.Reflections.map(function(reflection){
								return (
									<div className="reflection" key={reflection.ID}>
										<Link to={`/week-${this.state.props.ParentID}/day-${this.state.props.ID}/${reflection.ID}`}>
											{reflection.Title}
										</Link>
									</div>
								)
							}, this)}
							{this.state.props.Days.map(function(day){
								return <Day key={day.ID} {...day} />
							})}
						</div>
					)}
				/>
			</div>
		)
	}
}

export default Week;