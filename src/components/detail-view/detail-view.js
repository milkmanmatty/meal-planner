import React from 'react';


class DetailView extends React.Component {
	constructor(props){
		super(props);
		this.state = { 
			props: this.props
		}
	}

	componentWillReceiveProps(nextProps) {
		this.setState({ props: nextProps });
	}

	render(){
		if(this.state.props.meal){
			return (
				<div className="details">
					<h1>{this.state.props.meal.Title}</h1>
					<h3>Ingredients</h3>
					<div className="ingredients" dangerouslySetInnerHTML={{__html: this.state.props.meal.Ingredients}}></div>
					<h3>Preparation Method</h3>
					<div className="preparation_method" dangerouslySetInnerHTML={{__html: this.state.props.meal.PreparationMethod}}></div>
					<hr />
					<h3>Ate Correct Meal?: {this.state.props.meal.AteCorrectMeal === true ? "Yes" : "No"}</h3>
					{this.state.props.meal.MealAlterations && 
						<div>
							<h3>Alterations:</h3>
							<div className="alterations" dangerouslySetInnerHTML={{__html: this.state.props.meal.MealAlterations}}></div>
						</div>
					}
					
					<h3>Ate Different Meal?: {this.state.props.meal.AteDifferentMeal === true ? "Yes" : "No"}</h3>
					{this.state.props.meal.AlternativeConsumptionDetails !== "" &&
						<div>
							<h3>Alterations:</h3>
							<div className="alterations" dangerouslySetInnerHTML={{__html: this.state.props.meal.AlternativeConsumptionDetails}}></div>
						</div>}
				</div>
			)
		} else if(this.state.props.reflect){
			return (
				<div className="details">
					<h1>{this.state.props.reflect.Title}</h1>
					<div className="reflections" dangerouslySetInnerHTML={{__html: this.state.props.reflect.Reflections}}></div>
				</div>
			)
		} else {
			return (
				<div className="details"></div>
			)
		}
	}
}

export default DetailView;