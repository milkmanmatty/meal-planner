import React from 'react';
import { Link } from 'react-router-dom';

class Day extends React.Component {
	constructor(props){
		super(props);
		this.state = { 
			props: this.props
		}
	}

	componentWillReceiveProps(nextProps) {
		this.setState({ props: nextProps });
	}

	render(){
		return(
			<div className={this.state.props.IsOpen}>
				<h5>{this.state.props.Title}</h5>
				<ul>
				{this.state.props.Meals.map(function(meal){
					return (
						<li key={meal.ID}>
							<Link to={`/week-${this.state.props.ParentID}/day-${this.state.props.ID}/${meal.ID}`}>
								{meal.Title}
							</Link>
						</li>
					)
				}, this)}
				</ul>
			</div>
		)
	}
}

export default Day;