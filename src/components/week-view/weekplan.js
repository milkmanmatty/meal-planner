import React from 'react';
import DayPlan from './dayplan.js';
import '../../styles/weekplan.css';

class WeekPlan extends React.Component {
	constructor(props){
		super(props);
		this.state = { 
			props: this.props
		}
	}

	componentDidMount() {
		fetch(`https://meal.milkmanmatty.com.au/umbraco/api/Data/GetDays?w=${this.props.id}`)
		.then(res => res.json())
		.then(
			(result) => {
				this.setState({
					isLoaded: true,
					plans: result
				});
			},
			(error) => {
				this.setState({
					isLoaded: false,
					plans: null
				});
				console.log(error);
			}
		)
	}

    render(){
		if(this.state.isLoaded){
			return (
				<div className="WeekPlan">
					<h2>Week {this.props.weeknum}</h2>
					<div className="DayPlans">
						{this.state.plans.map(function(plan){
							return (
								<DayPlan 
									key={plan.ID}
									id={plan.ID} 
									title={plan.Title}
									weeks={plan.Children}
									parentid={this.props.id}
								/>
							);
						}, this)}
					</div>
				</div>
			);
		} else {
			return (
				<div className="WeekPlan">
					<h2>Loading...</h2>
					<div className="DayPlans"></div>
				</div>
			);
		}
	}
}

export default WeekPlan;
