import React from 'react';
import MealPlan from './mealplan.js';
import '../../styles/mealplan.css';

class MealPlans extends React.Component {
	constructor(props){
		super(props);
		this.state = { 
			props: this.props,
			plans: null,
			isLoaded: false
		}
	}

	componentDidMount() {
		fetch("https://meal.milkmanmatty.com.au/umbraco/api/Data/GetMealPlans")
		.then(res => res.json())
		.then(
			(result) => {
				this.setState({
					isLoaded: true,
					plans: result
				});
			},
			(error) => {
				this.setState({
					isLoaded: false,
					plans: null
				});
				console.log(error);
			}
		)
	}

	render(){
		if(this.state.isLoaded){
			return (
				<div className="MealPlans">
					{this.state.plans.map(function(plan){
						return (
							<MealPlan 
								key={plan.ID}
								id={plan.ID} 
								title={plan.Title}
								weeks={plan.Children}
							/>
						);
					})}
				</div>
			);
		} else {
			return (
				<div className="MealPlans">
					<p>Loading...</p>
				</div>
			);
		}
        
	}
}

export default MealPlans;