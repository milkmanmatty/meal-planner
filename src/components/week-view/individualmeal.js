import React from 'react';

function IndivMeal(props) {
	return (
		<div className="IndivMeal">
			<h4>{props.title}</h4>
		</div>
	);
}

export default IndivMeal;