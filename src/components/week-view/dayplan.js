import React from 'react';
import { Link } from 'react-router-dom';
import IndivMeal from './individualmeal.js';
import '../../styles/dayplan.css';

class DayPlan extends React.Component {
	constructor(props){
		super(props);
		this.state = { 
			props: this.props
		}
	}

	componentDidMount() {
		fetch(`https://meal.milkmanmatty.com.au/umbraco/api/Data/GetMeals?d=${this.props.id}`)
		.then(res => res.json())
		.then(
			(result) => {
				this.setState({
					isLoaded: true,
					plans: result
				});
			},
			(error) => {
				this.setState({
					isLoaded: false,
					plans: null
				});
				console.log(error);
			}
		)
	}

	render(){
		if(this.state.isLoaded){
			return (
				<div className="DayPlan">
					<h3>
						<Link to={`/week-${this.props.parentid}/day-${this.props.id}`}>
							{this.props.title}
						</Link>
					</h3>
					<div className="IndivMeals">
						{this.state.plans.map(function(plan){
							return (
								<IndivMeal 
									key={plan.ID}
									id={plan.ID} 
									title={plan.Title}
								/>
							);
						})}
					</div>
				</div>
			);
		} else {
			return (
				<div className="DayPlan">
					<h3>Loading...</h3>
					<div className="IndivMeals"></div>
				</div>
			);
		}
	}
}

export default DayPlan;