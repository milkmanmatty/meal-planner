import React from 'react';
import WeekPlan from './weekplan.js';
import '../../styles/mealplan.css';

class MealPlan extends React.Component {
	constructor(props){
		super(props);
		this.state = { 
			props: this.props,
			isLoaded: false
		}
	}

	componentDidMount() {
		fetch(`https://meal.milkmanmatty.com.au/umbraco/api/Data/GetWeeks?p=${this.props.id}`)
		.then(res => res.json())
		.then(
			(result) => {
				this.setState({
					isLoaded: true,
					plans: result
				});
			},
			(error) => {
				this.setState({
					isLoaded: false,
					plans: null
				});
				console.log(error);
			}
		)
	}

    render(){
		if(this.state.isLoaded){
			return (
				<div className="MealPlan">
					<h1>{this.props.title}</h1>
					<div className="WeekPlans">
					{this.state.plans.map(function(plan,index){
						return (
							<WeekPlan 
								key={plan.ID}
								id={plan.ID} 
								title={plan.Title}
								weeks={plan.Children}
								weeknum={index+1}
								parentid={this.props.ID}
							/>
						);
					}, this)}
					</div>
				</div>
			);
		} else {
			return (
				<div className="MealPlan">
					<h1>Loading...</h1>
					<div className="WeekPlans"></div>
				</div>
			);
		}
	}
}

export default MealPlan;
