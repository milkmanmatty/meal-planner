import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import MealPlans from './components/week-view/mealplans.js';
import MealPlanDetail from './components/detail-view/mealplan-detail.js';

function App() {
	return (
		<Router>
			<main>
				<Route exact={true} path="/" component={MealPlans} />
				<Route path="/week-:weekid/day-:dayid/:mealid?" component={MealPlanDetail} />
			</main>
		</Router>
	);
}

export default App;
